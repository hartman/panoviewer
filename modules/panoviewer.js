'use strict';

const api = new mw.Api();

function generateBadge( badgeText ) {
	const badge = document.createElement( 'span' );
	badge.className = 'mwe-panoviewer-badge';
	badge.textContent = badgeText;
	return badge;
}

class PanoViewer {
	constructor( img ) {
		this.placeholder = img;
		this.projectionType = img.getAttribute( 'data-file-projection' );
		this.stereoEncoding = img.getAttribute( 'data-file-stereo' );
		this.initPlaceHolder();
	}

	initPlaceHolder() {
		this.placeholder.classList.add( 'mwe-panoviewer' );
		this.placeholder.parentNode.style.position = 'relative';
		this.placeholder.parentNode.style.display = 'inline-block';

		if ( this.projectionType && this.projectionType !== 'cylindrical' ) {
			this.placeholder.parentNode.append( generateBadge( mw.msg( 'panoviewer-projectiontype-360' ) ) );
		}
		if ( this.projectionType === 'cylindrical' ) {
			this.placeholder.parentNode.append( generateBadge( mw.msg( 'panoviewer-projectiontype-pano' ) ) );
		}
		if ( this.stereoEncoding ) {
			this.placeholder.parentNode.append( generateBadge( mw.msg( 'panoviewer-projectiontype-stereo' ) ) );
		}

		this.placeholder.addEventListener( 'click', this.click.bind( this ) );
	}

	initViewer( titleInfo, commonsFilename ) {
		const viewer = document.createElement('div' );
		viewer.style.height = this.placeholder.getAttribute( 'height' ) + 'px';
		viewer.style.width = this.placeholder.getAttribute( 'width' ) + 'px';

		let config = {
			type: 'equirectangular',
			preview: this.placeholder.getAttribute('src'),
			panorama: titleInfo.query.pages[0].imageinfo[0].thumburl,
			// orientationOnByDefault: true,
			autoRotate: this.projectionType === 'equirectangular' ? -2 : 0,
			autoRotateInactivityDelay: 5000,
			autoLoad: true,
			showZoomCtrl: false,
			showFullscreenControl: true,
			crossOrigin: 'anonymous',
			strings: require( './panellum-translations.js' )
		};

		if ( this.projectionType === 'equirectangular' ) {
			this.pannellumViewer = window.pannellum.viewer( viewer, config );
		} else if ( this.projectionType === 'cylindrical' ) {
			config = Object.assign(config, {
				// We assume a 180degree view for cylindrical panoramas
				haov: 180,
				// haov: 360,
				vaov: 54
			});
			this.pannellumViewer = window.pannellum.viewer( viewer, config );
		}

		if ( !this.pannellumViewer ) {
			mw.log.error( 'Unable to load Panoviewer for: ' + this.placeholder.getAttribute('src') );
			return;
		}

		// We need an extra span, because the viewer overrides so many settings
		var span = document.createElement('span');
		span.className = 'mwe-panoviewer mw-file-element';
		span.style.height = this.placeholder.style.height + 'px';
		span.style.width = this.placeholder.style.width + 'px';
		span.appendChild( viewer );

		// Disable the MMV clickhandler on the <a>
		$( this.placeholder.parentNode).off( 'click' ).on( 'click', function ( e ) {
			e.preventDefault();
		} );
		this.placeholder.replaceWith( span );

		this.pannellumViewer.on( 'load', () => {
			Array.from( this.pannellumViewer.getContainer()
				.querySelectorAll( '.pnlm-control' ) )
				.map( ( control ) => {
					// Fixup accessibility of Pannellum buttons
					control.setAttribute( 'role', 'button' );
					if ( control.classList.contains( 'pnlm-fullscreen-toggle-button' ) ) {
						control.setAttribute( 'aria-label', mw.msg( 'pannellum-fullscreen-button' ) );
					}
					if ( control.classList.contains( 'pnlm-zoom-in' ) ) {
						control.setAttribute( 'aria-label', mw.msg( 'pannellum-zoomin-button' ) );
					}
					if ( control.classList.contains( 'pnlm-zoom-out' ) ) {
						control.setAttribute( 'aria-label', mw.msg( 'pannellum-zoomout-button' ) );
					}
				} );
		} );
	}

	click( e ) {
		if ( (e.button !== 0 && e.which !== 1) || e.altKey || e.ctrlKey || e.shiftKey || e.metaKey || e.replayed ) {
			// Ignore if not a plain click
			return;
		}
		// Prevent MMV from running
		e.preventDefault();
		e.stopImmediatePropagation();
		this.loadPanoViewer();
	}

	loadPanoViewer( e ) {
		const imgPath = new URL( this.placeholder.getAttribute('src'), document.baseURI ).pathname;
		let imgFilename = imgPath.substring( imgPath.lastIndexOf('/') + 1 );
		imgFilename = decodeURI( imgFilename.replace(/\d+px-/, ''));

		const pannellumPromise = mw.loader.using( [ 'ext.panoviewer.pannellum' ] );
		const infoPromise = api.get({
			action: 'query',
			titles: 'File:' + imgFilename,
			prop: 'imageinfo',
			iiprop: 'url',
			iiurlwidth: '6000',
			formatversion: 2
		} );

		$.when( infoPromise, pannellumPromise ).then( ( titleInfo )=> {
			if ( titleInfo[0] && titleInfo[0].query && titleInfo[0].query.pages.length > 0 ) {
				$( this.placeholder.parentNode ).find( '> .mwe-panoviewer-badge' ).remove()
				this.initViewer( titleInfo[0], imgFilename );
			}
		} );
	}
}

module.exports = PanoViewer;
