const PanoViewer = require( './panoviewer.js' );

function init( $content ) {
	$content
		.find( 'img[data-file-projection], img[data-file-stereo]' )
		.each( function( index, img ) {
			new PanoViewer( img );
		} );
}

mw.hook('wikipage.content').add( init );

$( function() {
	const imgElement = document.querySelector( '.fullImageLink img' );
	if ( imgElement ) {
		// imgElement.setAttribute('data-file-projection', 'equirectangular');
		// new PanoViewer(imgElement).loadPanoViewer();
	}
} );
