/* This array maps the MW translation strings to keys for the Pannellum config */
module.exports = {
	'loadButtonLabel': mw.msg( 'pannellum-loadbutton-label' ),
	'loadingLabel': mw.msg( 'pannellum-loading-label' ),
	'bylineLabel': mw.msg( 'pannellum-byline-label' ),
	'noPanoramaError': mw.msg( 'pannellum-nopanorama-error'),
	'fileAccessError': mw.msg( 'pannellum-fileaccess-error'),
	'malformedURLError': mw.msg( 'pannellum-malformedurl-error'),
	'iOS8WebGLError': mw.msg( 'pannellum-ios8webgl-error'),
	'genericWebGLError': mw.msg( 'pannellum-genericwebgl-error'),
	'textureSizeError': mw.msg( 'pannellum-texturesize-error'),
	'unknownError': mw.msg( 'pannellum-unknown-error'),
	'twoTouchActivate': mw.msg( 'pannellum-twotouch-activate' ),
	'twoTouchXActivate': mw.msg( 'pannellum-twotouchx-activate' ),
	'twoTouchYActivate': mw.msg( 'pannellum-twotouchy-activate' ),
	'ctrlZoomActivate': mw.msg( 'pannellum-ctrlzoom-activate')
}
