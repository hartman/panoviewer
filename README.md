--------------------------------------------------------------------------
README for the PanoViewer extension
Copyright © 2019 Derk-Jan Hartman and others
Licenses: MIT
--------------------------------------------------------------------------

The PanoViewer extension provides the ability to mark and view image files
using a JS based panorama viewer

The PanoViewer extension was originally written by Derk-Jan Hartman in
2019 and is released under the MIT license. The
internationalization files contain contributions by several people;
they are mentioned in each file individually.

Instructions on installing and using this extension are available at
<https://www.mediawiki.org/wiki/Extension:PanoViewer>
