<?php
/**
 * @license GPL-2.0-or-later
 * @author Derk-Jan Hartman
 */

$magicWords = [];

/** English
 * @author Derk-Jan Hartman
 */
$magicWords['en'] = [
	'panoviewer' => [ 0, 'panoviewer' ],
];
