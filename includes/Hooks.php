<?php
/**
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 * http://www.gnu.org/copyleft/gpl.html
 */
namespace MediaWiki\Extensions\PanoViewer;

use File;
use IContextSource;
use MediaWiki\Hook\ImageBeforeProduceHTMLHook;
use MediaWiki\Hook\ParserFirstCallInitHook;
use MediaWiki\Hook\ThumbnailBeforeProduceHTMLHook;
use MediaWiki\Page\Hook\ImageOpenShowImageInlineBeforeHook;
use Parser;

class Hooks implements
	ImageBeforeProduceHTMLHook,
	ThumbnailBeforeProduceHTMLHook,
	ImageOpenShowImageInlineBeforeHook,
	ParserFirstCallInitHook
{
	public const VERSION = 1.0;

	/**
	 * @param \DummyLinker &$dummy
	 * @param \Title &$title
	 * @param \File|bool &$file
	 * @param array &$frameParams
	 * @param array &$handlerParams
	 * @param string|bool &$time
	 * @param string|null &$result
	 * @param \Parser $parser
	 * @param string &$query
	 * @param int|null &$widthOption
	 */
	public function onImageBeforeProduceHTML (
		$dummy, &$title, &$file, &$frameParams,
		&$handlerParams, &$time, &$result, $parser, &$query, &$widthOption
	) {
		wfDebugLog( 'panoviewer', 'Image link: ' . $title );
		$parser->getOutput()->addModules( [ 'ext.panoviewer' ] );
	}

	public function onThumbnailBeforeProduceHTML(
		$thumbnail, &$attribs, &$linkAttribs
	) {
		$this->isPanoViewerFile( $thumbnail->getFile(), $attribs );
	}

	/**
	 * Hook handler for extended metadata
	 *
	 * @param array &$combinedMeta Metadata so far
	 * @param File $file The file object in question
	 * @param IContextSource $context Context. Used to select language
	 * @param bool $singleLang Get only target language, or all translations
	 * @param int &$maxCache How many seconds to cache the result
	 * @return bool This hook handler always returns true
	 */
	public static function onGetExtendedMetadata(
		&$combinedMeta, File $file, IContextSource $context, $singleLang, &$maxCache
	) {
		wfDebugLog( 'panoviewer', 'HIT EXT METADATA' );
		if (
			isset( $combinedMeta[ 'PanoViewerMetadataExtension' ][ 'value' ] )
			&& $combinedMeta[ 'PanoViewerMetadataExtension' ][ 'value' ] == self::VERSION
		) {
			// This is a file from a remote API repo, and CommonsMetadata is installed on
			// the remote as well, and generates the same metadata format. We have nothing to do.
			return true;
		} else {
			$combinedMeta[ 'PanoViewerMetadataExtension' ] = [
				'value' => self::VERSION,
				'source' => 'extension',
			];
		}

		return true;
	}

	/**
	 *  Hook to register any render callbacks with the parser
	 */
	public function onParserFirstCallInit( $parser ) {
		// Create a function hook associating the "panoviewer" magic word with configurePanorama()
		$parser->setFunctionHook( 'panoviewer', [ self::class, 'configurePanorama' ] );
	}

	/**
	 * Render the output of {{#example:}}.
	 */
	public static function configurePanorama( Parser $parser, $param1 = '', $param2 = '' ) {
		// The input parameters are wikitext with templates expanded.
		// The output should be wikitext too.
		$output = "param1 is $param1 and param2 is $param2";
		if ( !$param1 || !$param2 || !self::validatePanoViewerParams( $param1, $param2 ) ) {
			$output = "ERROR";
		}
		$parser->getOutput()->setExtensionData( 'panoviewer', 'test ' . $param1 . strtolower( $param2 ) );
//		$parser->getOutput()->setProperty( 'panoviewer_' . $param1, strtolower( $param2 ) );
		return $output;
	}

	private static function validatePanoViewerParams( $param1, $param2 ) {
		$allowedKeys = [
			'hfov',
			'minHfov',
			'maxHfov',
			'pitch',
			'minPitch',
			'maxPitch',
			'yaw',
			'minYaw',
			'maxYav',
			'haov',
			'vaov',
			'vOffset',
			'type',
		];
		if ( !in_array( $param1, $allowedKeys, true ) ) {
			return false;
		}
		return true;
	}

	public function onImageOpenShowImageInlineBefore( $imagePage, $output ): void {
		wfDebugLog('panoviewer', 'onImageOpenShowImageInlineBefore' );
		$isPanoViewerFile = $this->isPanoViewerFile( $imagePage->getFile() );
		if ( $isPanoViewerFile ) {
			$output->addModules( [ 'ext.panoviewer' ] );
		}
	}

	public function isPanoViewerFile( $file, &$attribs = [] ) {
		wfDebugLog( 'panoviewer', 'isPanoViewerFile: ' . $file->getTitle() );
		$panoViewerFile = false;
		$data = $file->getMetadataArray();
		wfDebugLog( 'panoviewer', print_r( $data, true) );

		if (
			isset( $data[ 'UsePanoramaViewer' ] ) &&
			isset( $data[ 'ProjectionType' ] ) &&
			$data[ 'UsePanoramaViewer' ] === 'True'
		) {
			$panoViewerFile = true;
			// GPano
			switch ( $data[ 'ProjectionType' ] ) {
				case 'equirectangular':
				default:
					// GPano only supports equirectangular so far
					wfDebugLog('Panoviewer', $data[ 'ProjectionType' ] );
					$attribs['data-file-projection'] = MediaProjectionType::EQUIRECTANGULAR;
			}
		}
		if ( ( $data[ 'CustomRendered' ] ?? 1 ) === 6 ) {
			// Apple iPhone panoramas
			wfDebugLog( 'Panoviewer', $data[ 'CustomRendered' ] );
			$attribs['data-file-projection'] = MediaProjectionType::CYLINDRICAL;
			$panoViewerFile = true;
		}

		// 3D files. We know that .jps files are stereo files
		if ( str_ends_with( $file->getName(), '.jps' ) || str_ends_with( $file->getName(), '.jps.jpg' ) ) {
			$attribs['data-file-stereo'] = MediaStereoEncoding::PARALLEL_LR;
			$panoViewerFile = true;
		}

		return $panoViewerFile;
	}
}
