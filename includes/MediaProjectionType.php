<?php
/**
 * Media Projection Types
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 * http://www.gnu.org/copyleft/gpl.html
 *
 * @file
 * @ingroup Media
 */

namespace MediaWiki\Extensions\PanoViewer;

abstract class MediaProjectionType {

	const STANDARD = 'standard';

	/**
	 * https://en.wikipedia.org/wiki/Equirectangular_projection
	 */
	const EQUIRECTANGULAR = 'equirectangular';

	/**
	 * Projection used by panorama mode of cameras
	 */
	const CYLINDRICAL = 'cylindrical';

	/**
	 * Equi-Angular Cubemap (EAC) projection
	 * Used especially by video
	 */
	const EAC = 'eac';

	/**
	 * Cubic map projection
	 * 6 square projection
	 *
	 *       up
	 * back, left, front, right
	 *       down
	 */
	const CUBE = 'cube';

	// Thgere are many other modes, but they are not used as much
	// const MERCATOR = 'mercator';
	// const MILLER = 'miller';
	// const LAMBERT = 'lambert';
	// const AZIMUTHAL = 'azimuthal';
	// const RECTILINEAR = 'rectilinear';
	// const FISHEYE = 'fisheye';
	// const EQUISOLID = 'equisolid'
	// const ORTHOGRAPHIC = 'ortographic';
	// const CUBIC = 'cubic';
	// const SINUSSOIDAL = 'sinusoidal';
}
