<?php

namespace MediaWiki\Extensions\PanoViewer;

/**
 * Encoding of stereoscopic images and video
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 * http://www.gnu.org/copyleft/gpl.html
 *
 * @file
 * @ingroup Media
 */
abstract class MediaStereoEncoding {

	const MONO = 'mono';

	/**
	 * Left and right are encoded in separate streams/tracks
	 */
	const DUAL_STREAM = 'dual';

	/**
	 * parallel
	 * Left image left 50%, right image right 50%
	 */
	const PARALLEL_LR = 'parallel_lr';

	/**
	 * parallel
	 * Left image right 50%, right image left 50%
	 */
	const PARALLEL_RL = 'parallel_rl';

	/**
	 * Left image top 50%, right image bottom 50%
	 */
	const PARALLEL_TB = 'parallel_tb';

	/**
	 * Left image bottom 50%, right image top 50%
	 */
	const PARALLEL_BT = 'parallel_bt';

	/**
	 * Left and right image lines are interleaved. top line is from right
	 */
	const INTERLACED_RL = 'interlaced_rl';

	/**
	 * Left and right image lines are interleaved. top line is from right
	 */
	const INTERLACED_LR = 'interlaced_lr';

	/**
	 * Color filtered single image Cyan/Red
	 */
	const ANAGLYPH_CR = 'anaglyph_cr';

	/**
	 * Color filtered single image Green/Magenta
	 */
	const ANAGLYPH_GM = 'anaglyph_gm';
}
